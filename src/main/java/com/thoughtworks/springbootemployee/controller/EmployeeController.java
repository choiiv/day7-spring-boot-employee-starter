package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        return employeeRepository.findById(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getAllEmployees(@RequestParam String gender) {
        return employeeRepository.findAllWithGender(gender);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.insert(employee);
    }

    @PutMapping("/{id}")
    public Employee updateEmployee(@PathVariable Long id ,@RequestBody Employee newEmployee){
        Employee originalEmployee= employeeRepository.findById(id);
        originalEmployee.setAge(newEmployee.getAge());
        originalEmployee.setSalary(newEmployee.getSalary());
        return originalEmployee;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeEmployee(@PathVariable Long id){
        employeeRepository.remove(id);
    }

    @GetMapping(params = {"pageNumber", "pageSize"})
    public List<Employee> getEmployeesByPage(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
        return employeeRepository.getEmployeesByPage(pageNumber, pageSize);
    }
}
