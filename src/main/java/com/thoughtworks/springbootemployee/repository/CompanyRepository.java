package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private List<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "disney"));
        companies.add(new Company(2L, "pixar"));
        companies.add(new Company(3L, "marvel"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Company> getCompaniesByPage(Integer pageNumber, Integer pageSize) {
        return companies.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company insert(Company company) {
        Long id = generateId();
        company.setId(id);
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max().orElse(0L) + 1;
    }

    public void remove(Long id) {
        if(findById(id) != null) {
            companies.remove(findById(id));
        }
    }
}
