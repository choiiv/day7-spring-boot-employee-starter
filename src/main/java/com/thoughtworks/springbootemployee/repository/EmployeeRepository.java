package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Lucy", 20, "Female", 2000, 1L));
        employees.add(new Employee(2L, "Lily", 20, "Female", 2000, 1L));
        employees.add(new Employee(3L, "Tom", 19, "Male", 2000, 2L));
        employees.add(new Employee(4L, "Ivan", 18, "Male", 2000, 2L));
    }
    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> findAllWithGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee insert(Employee employee) {
        Long id = generateId();
        employee.setId(id);
        employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream()
                .mapToLong(Employee::getId)
                .max().orElse(0L) + 1;
    }

    public void remove(Long id) {
        employees.removeIf(employee -> employee.getId().equals(id));
    }

    public List<Employee> getEmployeesByPage(Integer pageNumber, Integer pageSize) {
        return employees.stream()
                .skip((long) (pageNumber-1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Employee> findAllByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    public void removeEmployeesFromCompany(Long id) {
        employees = employees.stream()
                .filter(employee -> !employee.getCompanyId().equals(id))
                .collect(Collectors.toList());
    }
}
